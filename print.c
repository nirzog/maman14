#include "data.h";


void p_asm_cmd(asm_cmd_t *asCom){
    printf("address: %d, command: %s, num of args: %d,", asCom->address, asCom->cmd, asCom->args);

    p_bin_cmd_t(asCom->op);
    p_bin_cmd_t(asCom->cmd[0]);
    p_bin_cmd_t(asCom->cmd[1]);
    printf("\n");
}

void p_bin_cmd(bin_cmd_t binCmd){
    printf(" binary code: %d", binCmd.cmd);
}

void p_op_table(op_table_t *opTbale){
    printf("Command: %s Operand: %d, Function: %d, Args: %d, Access Mode: %d \n",
        opTbale->cmd, (int)opTbale->op, (int)opTbale->func, opTbale->args, opTbale->args, opTbale->acc_mode);
}

void p_sym_table(sym_table_t *symTable){
    printf("Sym: %s, Address: %d, Attribute: %d, Line: %d",
        symTable->sym, symTable->addr, (int)symTable->attr, symTable->line);
}


